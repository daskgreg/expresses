const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');

// Importing Routes
const authRoute = require('./path/auth');
const postRoute = require('./path/posts');

// ENV
dotenv.config();

// Connect To DB

 mongoose.connect(
     process.env.DB_CONNECT,
 { useUnifiedTopology: true },
 ()=> console.log('Connect to DB!')
 );


//  MiddleWares

app.use(cors());
app.use(express.json());


app.use('/api/user',authRoute);

app.use('/api/posts',postRoute);

app.listen(3000, () => console.log('Listening on port 3000'));
