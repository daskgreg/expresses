const path = require('express').Router();
const User = require('../model/User');
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken');

const { registerValidation, loginValidation } = require('../validation');

path.post('/register', async (req,res) =>{
    const {error} = registerValidation(req.body);
    // Validation of the data before making a user
    if(error) return res.status(400).send(error.details[0].message);
    // checking if the user is already in db
    const emailExists = await User.findOne({email: req.body.email});
    if(emailExists) return res.status(400).send('Email Already Exists');

    // Hash The Password 
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password, salt);

    //create user 
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashPassword
    });
    try{
        const savedUser = await user.save();
        res.send(savedUser);
    }catch(err){
        res.status(400).send(err);
    }
});
// Login 
path.post('/login', async (req,res) => {
    const {error} = loginValidation(req.body);
    // Validation of the data before making a user
    if(error) return res.status(400).send(error.details[0].message);
    // checking if email exists
    const user = await User.findOne({email: req.body.email});

    if(!user) return res.status(400).send('Email is wrong');

    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if(!validPassword) return res.status(400).send('Invalid Password');

    // Create and assign a token
   
    const token = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET);
    res.header('auth-token',token).send(token);
    res.status(200).send('SUCCESS');

    
});


module.exports = path;