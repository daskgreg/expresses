const path = require('express').Router();
const verify = require('./verifytoken');

path.get('/', verify , (req,res) => {
    res.json({
        posts: {
        title: 'My first post',
        desc: 'Random Data'
    }
});
});

module.exports = path;